import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomLibComponent } from './custom-lib.component';

const routes: Routes = [
  {
    path: '',
    component: CustomLibComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class CustomLibRoutingModule {}
