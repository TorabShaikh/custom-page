import { Component, OnInit } from '@angular/core';
import { DragulaService } from 'ng2-dragula';
import { CustomLibService } from './custom-lib.service';

@Component({
  selector: 'lib-custom-lib',
  template: `
    <p>custom-lib works! Current Time is {{ now }} and UUID is {{ id }}</p>
    <ul
      dragula="test"
      [dragulaModel]="pageList"
      (dragulaModelChange)="drop($event)"
    >
      <li *ngFor="let page of pageList; let i = index">
        {{ i }} {{ page.name }}
      </li>
    </ul>
  `,
  styles: [],
  providers: [DragulaService, CustomLibService]
})
export class CustomLibComponent implements OnInit {
  now: any = null;
  id: string;
  name = 'Angular';
  pageList = [
    { name: 'Item A' },
    { name: 'Item B' },
    { name: 'Item C' },
    { name: 'Item D' }
  ];
  constructor(
    private libService: CustomLibService,
    private dragulaService: DragulaService
  ) {
    console.log('In Library Component');
  }
  ngOnInit(): void {
    this.dragulaService.createGroup('test', {});
    this.dragulaService
      .dropModel('test')
      .subscribe((data: any) =>
        console.log('dropModel: ', JSON.stringify(data['targetModel'], null, 2))
      );
    setInterval(() => (this.now = this.libService.getCurrentTime()), 1000);
    this.id = this.libService.generateUUID();
  }
  drop(event: any): void {
    console.log('drop event: ', JSON.stringify(event, null, 2));
  }
}
