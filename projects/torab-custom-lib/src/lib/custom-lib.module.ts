import { NgModule } from '@angular/core';
import { CustomLibComponent } from './custom-lib.component';
import { CustomLibRoutingModule } from './custom-lib-routing.module';
import { DragulaModule } from 'ng2-dragula';
import { CommonModule } from '@angular/common';
@NgModule({
  declarations: [CustomLibComponent],
  imports: [
    CommonModule,
    DragulaModule.forRoot(),
    CustomLibRoutingModule,
  ],
  exports: [ CustomLibComponent ]
})
export class CustomLibModule { }
