import { Injectable } from '@angular/core';

@Injectable()
export class CustomLibService {
private a: any;
public b: any;

  constructor() {
    console.log('In Library service');
  }
  public printMyName(name: string): void {
    console.log('Your name is ', name);
  }
  public getCurrentTime(): any {
    return new Date();
  }
  public generateUUID(): string {
    // return uuid.v1();
    console.log(this.a, this.b);
    return String(Math.random() * 1000);
  }
}
