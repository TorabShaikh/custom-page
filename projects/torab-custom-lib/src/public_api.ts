/*
 * Public API Surface of torab-custom-lib
 */

export * from './lib/custom-lib.service';
export * from './lib/custom-lib.component';
export * from './lib/custom-lib.module';
